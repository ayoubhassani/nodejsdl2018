//module.exports = [3,2,1];

var express=require('express');
var router=express();
var forum=require('../dbModel/forum')
var service = require('../service/service')

router.get('/',(req,res)=>{
    service.listForum((data)=>
    {
        res.json(data)
    })
})
router.get('/:id',(req,res)=>{
    service.getForum(req.params.id,(data)=>
    {
        res.json(data)
    })
})
router.post('/',(req,res)=>{
    service.insertForum(req.body,(data)=>{
        res.json(data)
    })

})
router.put('/:id',(req,res)=>{
    service.updateForum(req.params.id,req.body,(data)=>{
        res.json(data);
    })
})
router.delete('/:id',(req,res)=>{
    service.deleteForum(req.params.id,(data)=>
    {
        res.json(data)
    })
})

module.exports=router;