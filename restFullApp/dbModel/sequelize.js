const Sequelize = require('sequelize');
const sequelize = new Sequelize('restapp_db', 'root', '', {
    host: 'localhost',
    // type de db
    dialect: 'mysql',
    port:3306,
// nb de connexion
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },

    // SQLite only
   // storage: 'path/to/database.sqlite'
});

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });


module.exports=sequelize;