const Sequelize = require('sequelize');
const sequelize = require('./sequelize');


// Attention, il aura comme valeur users
const User = sequelize.define('user', {
    nom: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    },
    avatar: {
        type: Sequelize.STRING
    }
});




// force: true will drop the table if it already exists
/*User.sync({force: true}).then(() => {
    // Table created
    return User.create({
        nom: 'John',
        email: 'Hancock'
    });
});*/

module.exports=User;