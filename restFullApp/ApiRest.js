//console.log(1) permet de vérifier que l'appli est bien appelée
var express=require('express');
var cors=require('cors');
var app=express();
var ForumCtler = require('./controller/ForumController');

app.use(cors());
app.use(express.json()); // ajoute la propriété body à la requête
app.use('/forum',ForumCtler);

/*app.get('/',(req,res)=> {
    console.log(ForumCtler);
    res.send('ok');
})*/
// réponse au navigateur
/* app.use(function (req,res,next) {
    res.send('it&apos;s working');
})*/

app.listen(1000)