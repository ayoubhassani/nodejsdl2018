var forum=require('../dbModel/forum')
module.exports = {
    listForum: function(callback) {
        forum.findAll().then((forumslLists)=>{
            callback(forumslLists)
        })
    },
    getForum:function(id,callback){
        forum.findOne({where:{id: id}}).then((unForum)=> {
            callback(unForum)
        })
    },
    insertForum: function (data,callback) {
        forum.create(data).then((inserted)=> {
            callback(inserted)
        })

    },
    updateForum: function (id,data,callback) {
        forum.update(data,{where:{id: id}}).then((args)=> {
            callback(args)
        })

    },
    deleteForum: function (id,callback) {
        forum.destroy({where:{id: id}}).then((args)=> {
            callback(args)
        })

    }
}