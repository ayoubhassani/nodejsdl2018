// get the client
const mysql = require('mysql2');

// create the connection to database
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root'
});

// simple query
connection.query(
    'select * from projnode_db.users;',
    function(err, results, fields) {
        if (err) throw err;
        console.log(results); // results contains rows returned by server
        console.log(fields); // fields contains extra meta data about results, if available
    }
);
class User {
    getAll(callback) {
        connection.query(
            'select * from projnode_db.users;',
            function (err, results, fields) {
                if (err) throw err;
                console.log(results); // results contains rows returned by server
                console.log(fields); // fields contains extra meta data about results, if available
                callback(err, results)
            }
        );

    }

    insert(data, callback) {
        connection.query(
            'insert into projnode_db.users (`name`,`email`) values(?,?)',
            [data.name, data.email],
            function (err, results, fields) {
                callback(err, results)
            }
        );
    }
    update(data, callback) {
        connection.query(
            'update projnode_db.users set `name`=? ,`email`=? where id=?',
            [data.name, data.email,data.id],
            function (err, results, fields) {
                callback(err, results)
            }
        );
    }
    delete(id, callback) {
        connection.query(
            'delete from projnode_db.users where id=?',
            [id],
            function (err, results, fields) {
                callback(err, results)
            }
        );
    }
    getone(id, callback) {
        connection.query(
            'select * projnode_db.users where id=?',
            [id],
            function (err, results, fields) {
                callback(err, results)
            }
        );
    }
}
module.exports = new User()