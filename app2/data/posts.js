// get the client
const mysql = require('mysql2');

// create the connection to database
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root'
});

// simple query
connection.query(
    'select * from projnode_db.posts;',

    function(err, results, fields) {
        if (err) throw err;
        console.log(results); // results contains rows returned by server
        console.log(fields); // fields contains extra meta data about results, if available
    }
);
class Post {

    getAll(callback) {
        connection.query(
            'select * from projnode_db.posts;',
            function (err, results, fields) {
                if (err) throw err;
                console.log(results); // results contains rows returned by server
                console.log(fields); // fields contains extra meta data about results, if available
                callback(err, results)
            }
        );

    }

    insert(data, callback) {
        connection.query(
            'insert into projnode_db.posts (`userId`,`title`,`body`) values(?,?,?)',
            [data.userId, data.title, data.body],
            function (err, results, fields) {
                callback(err, results)
            }
        );
    }
    update(data, callback) {
        connection.query(
            'update projnode_db.posts set `title`=? ,`body`=? where id=?',
            [data.title, data.body,data.id],
            function (err, results, fields) {
                callback(err, results)
            }
        );
    }
    delete(id, callback) {
        connection.query(
            'delete from projnode_db.posts where id=?',
            [id],
            function (err, results, fields) {
                callback(err, results)
            }
        );
    }

    getOne(id, callback) {
        connection.query(
            'select * from projnode_db.posts where id=?',
            [id],
            function (err, results, fields) {
                callback(err, results)
            }
        );
    }
}
module.exports = new Post()