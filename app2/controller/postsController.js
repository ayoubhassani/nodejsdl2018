var exp  = require('express')
const rooter=exp();
// il est possible de faire rooter.all pour appliquer sur toutes les méthodes
var postservice=require('../data/posts')

rooter.get('/',(req, res)=> {
    postservice.getAll((err,resultats)=>{
        if(err) res.status(500).send(err)
        else res.json(resultats)
    })
    //  res.send('get posts')

})
rooter.put('/',(req, res)=> {
    postservice.insert(req.body,(err,resultats)=>{
        if(err) res.status(500).send(err)
        else res.status(201).json(resultats)
    })

})


rooter.post('/',(req, res)=> {
    postservice.update(req.body,(err,resultats)=>{
        if(err) res.status(500).send(err)
        else res.status(202).json(resultats)
    })

})

//rooter.delete('/',(req, res)=> {
rooter.delete('/:id',(req, res)=> {
    postservice.delete(req.params.id,(err,resultats)=>{
        if(err) res.status(500).send(err)
        else res.status(200).json(resultats)
    })

})
rooter.get('/:id', (req, res)=> {
    postservice.getOne(req.params.id,(err, resultats)=> {
        if (err) res.status(500).send(err)
        else res.status(200).json(resultats)
    })

})



module.exports=rooter;