var exp  = require('express')
const rooter=exp();
// il est possible de faire rooter.all pour appliquer sur toutes les méthodes
var userservice=require('../data/users')

rooter.get('/',(req, res)=> {
        userservice.getAll((err,resultats)=>{
                if(err) res.status(500).send(err)
                else res.json(resultats)
    })
      //  res.send('get Users')

})
rooter.put('/',(req, res)=> {
    userservice.insert(req.body,(err,resultats)=>{
        if(err) res.status(500).send(err)
        else res.status(201).json(resultats)
    })

})


rooter.post('/',(req, res)=> {
    userservice.update(req.body,(err,resultats)=>{
        if(err) res.status(500).send(err)
        else res.status(202).json(resultats)
    })

})

//rooter.delete('/',(req, res)=> {
rooter.delete('/:id',(req, res)=> {
    userservice.delete(req.params.id,(err,resultats)=>{
        if(err) res.status(500).send(err)
        else res.status(200).json(resultats)
    })

})
rooter.get('/:id',(req, res)=> {
    userservice.getone(req.params.id,(err,resultats)=>{
        if(err) res.status(500).send(err)
        else res.status(200).json(resultats)
    })

})
module.exports=rooter;