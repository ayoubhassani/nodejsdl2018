var express = require('express')

var app=express()
var UserController=require('./controller/users.js')
var PostController=require('./controller/posts.js')
app.use(express.static('static'));
app.use('/users',UserController);
app.use('/posts',PostController);
app.get('/',function (request, response)
{response.send('Hello')
})

app.delete('/',function (request, response)
{response.send('delete')
})

app.post('/',function (request, response)
{response.send('post')
})

app.put('/',function (request, response)
{response.send('put')
})

app.listen(9090)